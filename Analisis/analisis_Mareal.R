#Título del script: Gráfica del cambio mareal
#Autores: Christian Bermúdez-Rivas 
#Objetivo: construir la gráfica de la onda mareal en el área de bahía de Málaga y Buenaventura
#Lenguaje: R
#Fecha: Junio 2022
#Notas: No olvidar instalar los paquetes necesarios para correr el script
###############################################################################################################################

####Librería####
library(tidyverse)
library(lubridate)
library(dplyr)
library(ggrepel)
library(scales)
library(gridExtra)

today()
now()



####Entrada de datos####
marea<-read.table("Datos_Marea.csv", header=TRUE, sep = ",")


marea <- dplyr::mutate(marea, fecha_Hora= paste(Fecha, Hora)) 

marea$fecha_Hora<- ymd_hms(marea$fecha_Hora)
marea$fecha_Hora<-   as.POSIXct(marea$fecha_Hora)




head(marea)

estaciones<-read.table("Datos_colecta.csv", header=TRUE, sep = ",")

estaciones <- mutate(estaciones, fecha_Hora = paste(Fecha, Hora)) 
estaciones$fecha_Hora<- ymd_hms(estaciones$fecha_Hora)
estaciones$fecha_Hora<- as.POSIXct(estaciones$fecha_Hora)

estaciones$Densidad<-as.factor(estaciones$Densidad)

write.table(estaciones, "Alturas_Mareales_Estaciones.csv", sep = ",", col.names = TRUE)


ciclo_mareal<-function(datos, fecha_hora, altura){
  ggplot()+
    geom_line(data=datos, aes(x=fecha_hora, y=altura),size=1, colour="grey")+
    geom_hline(yintercept = -1:1,linetype='dotted', col = 'red')+
    labs(x = "Fecha", y = "Altura mareal referída al MLWS [m]") +
    theme_classic()+
    scale_x_datetime(
      breaks = seq(as.POSIXct("2021-06-06 18:00:00"),
                   as.POSIXct("2021-06-08 18:00:00"), "6 hours"),
      labels = date_format("%a-%d\n%H:%M", tz = ""),
      expand = c(0, 0),
      limits = c(
        as.POSIXct("2021-06-06 18:00:00"),
        as.POSIXct("2021-06-08 18:00:00")))
}



####Construcción de la gráfica####
ciclo<-ciclo_mareal(marea, marea$fecha_Hora, marea$Altura)



Onda<-ciclo+
  geom_point(data=estaciones, aes(x=as.POSIXct(fecha_Hora), y=Altura,color = "red", size = 4))+
  geom_label_repel(aes(x=as.POSIXct(estaciones$fecha_Hora), y=estaciones$Altura,label = estaciones$Estacion),box.padding   = 0.35, 
                   point.padding = 0.5,
                   segment.color = 'black')+
  theme(legend.position = "none")




densidadPlot<-ciclo+
  geom_point(data=estaciones, aes(x=as.POSIXct(fecha_Hora), y=Altura),color = "#98F5FF", size = 5)+
  geom_label_repel(aes(x=as.POSIXct(estaciones$fecha_Hora), y=estaciones$Altura,label = estaciones$Densidad),box.padding   = 0.35, 
                   point.padding = 0.5,
                   segment.color = 'black')+
  theme(legend.position = "none")

png(filename ="../Imagenes/onda2.png", width = 1200, height = 300, res = "300", units = "px", pointsize = 12)
grid.arrange(Onda, ncol=1)
dev.off()

png(filename ="../Imagenes/densidadPlot.png", width = 2500, height = 1500, res = "300", units = "px", pointsize = 12)
grid.arrange(densidadPlot, ncol=1)
dev.off()

