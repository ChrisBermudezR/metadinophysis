# Análisis Estadísticos

En este ***notebook*** se presentan de manera reproducible los análisis estadísticos del artículo "**PRIMER REGISTRO DEL GÉNERO DE DINOFLAGELADO TECADO METADINOPHYSIS NIE & WANG (DINOPHYCEAE: DINOPHYSIALES) Y ANÁLISIS DE SU ABUNDANCIA EN ÁREAS COSTERAS DEL PACIFICO COLOMBIANO**". 

## Librerías

Librerías usadas en este análisis.


```python
%matplotlib inline
import mermaid
import pandas as pd
from scipy import stats
import matplotlib.pyplot as plt
from libpysal.weights.contiguity import Queen
from libpysal.examples import get_path
import numpy as np
import pandas as pd
import geopandas as gpd
import os
import splot
from dbfread import DBF
from splot.esda import moran_scatterplot
from esda.moran import Moran
from splot.esda import plot_moran

```

# Análisis de autocorrelación espacial de la abundancia

Los análisis de autocorrelación espacial de la abundancia de *Metadinophysis* sp. en las bahías de Buenaventura y Málaga tienen como objetivo comprobar si esta variable presenta algún tipo de estructura espacial. Para esto se realizó un análisis calculando el índice de Moran (I de Moran) para a abundancia en cada evento mareal en el área de estudio.


### Marea Baja

A continuación se cargaran los datos de abundancia de *Metadinophysis* en marea baja para ambas bahías.

%reload_ext mermaid
```mermaid
graph TD;
    A-->B;
    A-->C;
    B-->D;
    C-->D;
```


```python
Bajagdf = gpd.read_file('E:/ARTICULOS/metadinophysis/SIG_Data/abundancia_Baja.dbf')

Bajagdf

```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>OBJECTID</th>
      <th>Longitud</th>
      <th>Latitud</th>
      <th>Estaciones</th>
      <th>Marea</th>
      <th>ABUN</th>
      <th>Estacion_N</th>
      <th>Bahia</th>
      <th>geometry</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>-77.32536</td>
      <td>3.97272</td>
      <td>M1B</td>
      <td>Baja</td>
      <td>0</td>
      <td>1</td>
      <td>Malaga</td>
      <td>POINT (-77.32536 3.97272)</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>-77.32061</td>
      <td>3.89664</td>
      <td>M2B</td>
      <td>Baja</td>
      <td>100</td>
      <td>2</td>
      <td>Malaga</td>
      <td>POINT (-77.32061 3.89664)</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>-77.35525</td>
      <td>3.90114</td>
      <td>M3B</td>
      <td>Baja</td>
      <td>0</td>
      <td>3</td>
      <td>Malaga</td>
      <td>POINT (-77.35525 3.90114)</td>
    </tr>
    <tr>
      <th>3</th>
      <td>4</td>
      <td>-77.32536</td>
      <td>3.92775</td>
      <td>M4B</td>
      <td>Baja</td>
      <td>60</td>
      <td>4</td>
      <td>Malaga</td>
      <td>POINT (-77.32536 3.92775)</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5</td>
      <td>-77.33739</td>
      <td>3.93908</td>
      <td>M5B</td>
      <td>Baja</td>
      <td>20</td>
      <td>5</td>
      <td>Malaga</td>
      <td>POINT (-77.33739 3.93908)</td>
    </tr>
    <tr>
      <th>5</th>
      <td>6</td>
      <td>-77.27608</td>
      <td>3.99153</td>
      <td>M6B</td>
      <td>Baja</td>
      <td>160</td>
      <td>6</td>
      <td>Malaga</td>
      <td>POINT (-77.27608 3.99153)</td>
    </tr>
    <tr>
      <th>6</th>
      <td>7</td>
      <td>-77.06667</td>
      <td>3.86667</td>
      <td>B1B</td>
      <td>Baja</td>
      <td>0</td>
      <td>1</td>
      <td>Buenaventura</td>
      <td>POINT (-77.06667 3.86667)</td>
    </tr>
    <tr>
      <th>7</th>
      <td>8</td>
      <td>-77.06750</td>
      <td>3.89417</td>
      <td>B2B</td>
      <td>Baja</td>
      <td>20</td>
      <td>2</td>
      <td>Buenaventura</td>
      <td>POINT (-77.06750 3.89417)</td>
    </tr>
    <tr>
      <th>8</th>
      <td>9</td>
      <td>-77.08167</td>
      <td>3.89200</td>
      <td>B3B</td>
      <td>Baja</td>
      <td>0</td>
      <td>3</td>
      <td>Buenaventura</td>
      <td>POINT (-77.08167 3.89200)</td>
    </tr>
    <tr>
      <th>9</th>
      <td>10</td>
      <td>-77.11167</td>
      <td>3.86000</td>
      <td>B4B</td>
      <td>Baja</td>
      <td>20</td>
      <td>4</td>
      <td>Buenaventura</td>
      <td>POINT (-77.11167 3.86000)</td>
    </tr>
    <tr>
      <th>10</th>
      <td>11</td>
      <td>-77.13200</td>
      <td>3.84917</td>
      <td>B5B</td>
      <td>Baja</td>
      <td>20</td>
      <td>5</td>
      <td>Buenaventura</td>
      <td>POINT (-77.13200 3.84917)</td>
    </tr>
    <tr>
      <th>11</th>
      <td>12</td>
      <td>-77.14550</td>
      <td>3.84083</td>
      <td>B6B</td>
      <td>Baja</td>
      <td>0</td>
      <td>6</td>
      <td>Buenaventura</td>
      <td>POINT (-77.14550 3.84083)</td>
    </tr>
    <tr>
      <th>12</th>
      <td>13</td>
      <td>-77.14033</td>
      <td>3.82083</td>
      <td>B7B</td>
      <td>Baja</td>
      <td>0</td>
      <td>7</td>
      <td>Buenaventura</td>
      <td>POINT (-77.14033 3.82083)</td>
    </tr>
    <tr>
      <th>13</th>
      <td>14</td>
      <td>-77.06194</td>
      <td>3.88972</td>
      <td>B8B</td>
      <td>Baja</td>
      <td>0</td>
      <td>8</td>
      <td>Buenaventura</td>
      <td>POINT (-77.06194 3.88972)</td>
    </tr>
  </tbody>
</table>
</div>




```python
abunBaja = Bajagdf['ABUN'].values
wBaja = Queen.from_dataframe(Bajagdf)
wBaja.transform = 'r'

```


```python
wBaja = Queen.from_dataframe(Bajagdf)
moranBaja = Moran(abunBaja, wBaja)
moranBaja.I

```




    0.04422827496757458




```python
fig, axbaja = moran_scatterplot(moranBaja, aspect_equal=True)
plt.show()


```


    
![png](output_10_0.png)
    



```python
plot_moran(moranBaja, zstandard=True, figsize=(10,4))
plt.show()

```


    
![png](output_11_0.png)
    



```python
moranBaja.p_sim

```




    0.17




```python
Altagdf = gpd.read_file('E:/ARTICULOS/metadinophysis/SIG_Data/abundancia_Alta.dbf')
Altagdf

```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>OBJECTID</th>
      <th>Longitud</th>
      <th>Latitud</th>
      <th>Estaciones</th>
      <th>Marea</th>
      <th>ABUN</th>
      <th>Bahia</th>
      <th>Estacion_N</th>
      <th>geometry</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>-77.32536</td>
      <td>3.97272</td>
      <td>M1A</td>
      <td>Alta</td>
      <td>140</td>
      <td>Malaga</td>
      <td>1</td>
      <td>POINT Z (-77.32536 3.97272 140.00000)</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>-77.32061</td>
      <td>3.89664</td>
      <td>M2A</td>
      <td>Alta</td>
      <td>140</td>
      <td>Malaga</td>
      <td>2</td>
      <td>POINT Z (-77.32061 3.89664 140.00000)</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>-77.35525</td>
      <td>3.90114</td>
      <td>M3A</td>
      <td>Alta</td>
      <td>60</td>
      <td>Malaga</td>
      <td>3</td>
      <td>POINT Z (-77.35525 3.90114 60.00000)</td>
    </tr>
    <tr>
      <th>3</th>
      <td>4</td>
      <td>-77.32536</td>
      <td>3.92775</td>
      <td>M4A</td>
      <td>Alta</td>
      <td>60</td>
      <td>Malaga</td>
      <td>4</td>
      <td>POINT Z (-77.32536 3.92775 60.00000)</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5</td>
      <td>-77.33739</td>
      <td>3.93908</td>
      <td>M5A</td>
      <td>Alta</td>
      <td>80</td>
      <td>Malaga</td>
      <td>5</td>
      <td>POINT Z (-77.33739 3.93908 80.00000)</td>
    </tr>
    <tr>
      <th>5</th>
      <td>6</td>
      <td>-77.27608</td>
      <td>3.99153</td>
      <td>M6A</td>
      <td>Alta</td>
      <td>200</td>
      <td>Malaga</td>
      <td>6</td>
      <td>POINT Z (-77.27608 3.99153 200.00000)</td>
    </tr>
    <tr>
      <th>6</th>
      <td>7</td>
      <td>-77.06667</td>
      <td>3.86667</td>
      <td>B1A</td>
      <td>Alta</td>
      <td>240</td>
      <td>Buenaventura</td>
      <td>1</td>
      <td>POINT Z (-77.06667 3.86667 240.00000)</td>
    </tr>
    <tr>
      <th>7</th>
      <td>8</td>
      <td>-77.06750</td>
      <td>3.89417</td>
      <td>B2A</td>
      <td>Alta</td>
      <td>300</td>
      <td>Buenaventura</td>
      <td>2</td>
      <td>POINT Z (-77.06750 3.89417 300.00000)</td>
    </tr>
    <tr>
      <th>8</th>
      <td>9</td>
      <td>-77.08167</td>
      <td>3.89200</td>
      <td>B3A</td>
      <td>Alta</td>
      <td>40</td>
      <td>Buenaventura</td>
      <td>3</td>
      <td>POINT Z (-77.08167 3.89200 40.00000)</td>
    </tr>
    <tr>
      <th>9</th>
      <td>10</td>
      <td>-77.11167</td>
      <td>3.86000</td>
      <td>B4A</td>
      <td>Alta</td>
      <td>40</td>
      <td>Buenaventura</td>
      <td>4</td>
      <td>POINT Z (-77.11167 3.86000 40.00000)</td>
    </tr>
    <tr>
      <th>10</th>
      <td>11</td>
      <td>-77.13200</td>
      <td>3.84917</td>
      <td>B5A</td>
      <td>Alta</td>
      <td>860</td>
      <td>Buenaventura</td>
      <td>5</td>
      <td>POINT Z (-77.13200 3.84917 860.00000)</td>
    </tr>
    <tr>
      <th>11</th>
      <td>12</td>
      <td>-77.14550</td>
      <td>3.84083</td>
      <td>B6A</td>
      <td>Alta</td>
      <td>140</td>
      <td>Buenaventura</td>
      <td>6</td>
      <td>POINT Z (-77.14550 3.84083 140.00000)</td>
    </tr>
    <tr>
      <th>12</th>
      <td>13</td>
      <td>-77.14033</td>
      <td>3.82083</td>
      <td>B7A</td>
      <td>Alta</td>
      <td>20</td>
      <td>Buenaventura</td>
      <td>7</td>
      <td>POINT Z (-77.14033 3.82083 20.00000)</td>
    </tr>
    <tr>
      <th>13</th>
      <td>14</td>
      <td>-77.06194</td>
      <td>3.88972</td>
      <td>B8A</td>
      <td>Alta</td>
      <td>340</td>
      <td>Buenaventura</td>
      <td>8</td>
      <td>POINT Z (-77.06194 3.88972 340.00000)</td>
    </tr>
  </tbody>
</table>
</div>




```python
abunAlta = Altagdf['ABUN'].values
wAlta = Queen.from_dataframe(Altagdf)
wAlta.transform = 'r'
```


```python
wAlta = Queen.from_dataframe(Altagdf)
moranAlta = Moran(abunAlta, wAlta)
moranAlta.I
```




    -0.16013476796000436




```python
fig, ax = moran_scatterplot(moranAlta, aspect_equal=True)
plt.show()
```


    
![png](output_16_0.png)
    



```python
plot_moran(moranAlta, zstandard=True, figsize=(10,4))
plt.show()
```


    
![png](output_17_0.png)
    



```python
moranAlta.p_sim
```




    0.264




```python
print("Diferencias entre Mareas")
print(stats.mannwhitneyu(Altagdf['ABUN'], Bajagdf['ABUN'], use_continuity=True, alternative='two-sided', axis=0, method='auto'))

```

    Diferencias entre Mareas
    MannwhitneyuResult(statistic=175.0, pvalue=0.00037567384401513895)
    


```python
AltaMalaga = Altagdf[Altagdf['Bahia']=='Malaga']
BajaMalaga = Bajagdf[Bajagdf['Bahia']=="Malaga"]
Malaga=pd.concat([AltaMalaga, BajaMalaga])
Malaga
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>OBJECTID</th>
      <th>Longitud</th>
      <th>Latitud</th>
      <th>Estaciones</th>
      <th>Marea</th>
      <th>ABUN</th>
      <th>Bahia</th>
      <th>Estacion_N</th>
      <th>geometry</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>-77.32536</td>
      <td>3.97272</td>
      <td>M1A</td>
      <td>Alta</td>
      <td>140</td>
      <td>Malaga</td>
      <td>1</td>
      <td>POINT Z (-77.32536 3.97272 140.00000)</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>-77.32061</td>
      <td>3.89664</td>
      <td>M2A</td>
      <td>Alta</td>
      <td>140</td>
      <td>Malaga</td>
      <td>2</td>
      <td>POINT Z (-77.32061 3.89664 140.00000)</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>-77.35525</td>
      <td>3.90114</td>
      <td>M3A</td>
      <td>Alta</td>
      <td>60</td>
      <td>Malaga</td>
      <td>3</td>
      <td>POINT Z (-77.35525 3.90114 60.00000)</td>
    </tr>
    <tr>
      <th>3</th>
      <td>4</td>
      <td>-77.32536</td>
      <td>3.92775</td>
      <td>M4A</td>
      <td>Alta</td>
      <td>60</td>
      <td>Malaga</td>
      <td>4</td>
      <td>POINT Z (-77.32536 3.92775 60.00000)</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5</td>
      <td>-77.33739</td>
      <td>3.93908</td>
      <td>M5A</td>
      <td>Alta</td>
      <td>80</td>
      <td>Malaga</td>
      <td>5</td>
      <td>POINT Z (-77.33739 3.93908 80.00000)</td>
    </tr>
    <tr>
      <th>5</th>
      <td>6</td>
      <td>-77.27608</td>
      <td>3.99153</td>
      <td>M6A</td>
      <td>Alta</td>
      <td>200</td>
      <td>Malaga</td>
      <td>6</td>
      <td>POINT Z (-77.27608 3.99153 200.00000)</td>
    </tr>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>-77.32536</td>
      <td>3.97272</td>
      <td>M1B</td>
      <td>Baja</td>
      <td>0</td>
      <td>Malaga</td>
      <td>1</td>
      <td>POINT (-77.32536 3.97272)</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>-77.32061</td>
      <td>3.89664</td>
      <td>M2B</td>
      <td>Baja</td>
      <td>100</td>
      <td>Malaga</td>
      <td>2</td>
      <td>POINT (-77.32061 3.89664)</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>-77.35525</td>
      <td>3.90114</td>
      <td>M3B</td>
      <td>Baja</td>
      <td>0</td>
      <td>Malaga</td>
      <td>3</td>
      <td>POINT (-77.35525 3.90114)</td>
    </tr>
    <tr>
      <th>3</th>
      <td>4</td>
      <td>-77.32536</td>
      <td>3.92775</td>
      <td>M4B</td>
      <td>Baja</td>
      <td>60</td>
      <td>Malaga</td>
      <td>4</td>
      <td>POINT (-77.32536 3.92775)</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5</td>
      <td>-77.33739</td>
      <td>3.93908</td>
      <td>M5B</td>
      <td>Baja</td>
      <td>20</td>
      <td>Malaga</td>
      <td>5</td>
      <td>POINT (-77.33739 3.93908)</td>
    </tr>
    <tr>
      <th>5</th>
      <td>6</td>
      <td>-77.27608</td>
      <td>3.99153</td>
      <td>M6B</td>
      <td>Baja</td>
      <td>160</td>
      <td>Malaga</td>
      <td>6</td>
      <td>POINT (-77.27608 3.99153)</td>
    </tr>
  </tbody>
</table>
</div>




```python
AltaBuenaventura = Altagdf[Altagdf['Bahia']=='Buenaventura']
BajaBuenaventura = Bajagdf[Bajagdf['Bahia']=="Buenaventura"]
Buenaventura=pd.concat([AltaBuenaventura, BajaBuenaventura])
Buenaventura
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>OBJECTID</th>
      <th>Longitud</th>
      <th>Latitud</th>
      <th>Estaciones</th>
      <th>Marea</th>
      <th>ABUN</th>
      <th>Bahia</th>
      <th>Estacion_N</th>
      <th>geometry</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>6</th>
      <td>7</td>
      <td>-77.06667</td>
      <td>3.86667</td>
      <td>B1A</td>
      <td>Alta</td>
      <td>240</td>
      <td>Buenaventura</td>
      <td>1</td>
      <td>POINT Z (-77.06667 3.86667 240.00000)</td>
    </tr>
    <tr>
      <th>7</th>
      <td>8</td>
      <td>-77.06750</td>
      <td>3.89417</td>
      <td>B2A</td>
      <td>Alta</td>
      <td>300</td>
      <td>Buenaventura</td>
      <td>2</td>
      <td>POINT Z (-77.06750 3.89417 300.00000)</td>
    </tr>
    <tr>
      <th>8</th>
      <td>9</td>
      <td>-77.08167</td>
      <td>3.89200</td>
      <td>B3A</td>
      <td>Alta</td>
      <td>40</td>
      <td>Buenaventura</td>
      <td>3</td>
      <td>POINT Z (-77.08167 3.89200 40.00000)</td>
    </tr>
    <tr>
      <th>9</th>
      <td>10</td>
      <td>-77.11167</td>
      <td>3.86000</td>
      <td>B4A</td>
      <td>Alta</td>
      <td>40</td>
      <td>Buenaventura</td>
      <td>4</td>
      <td>POINT Z (-77.11167 3.86000 40.00000)</td>
    </tr>
    <tr>
      <th>10</th>
      <td>11</td>
      <td>-77.13200</td>
      <td>3.84917</td>
      <td>B5A</td>
      <td>Alta</td>
      <td>860</td>
      <td>Buenaventura</td>
      <td>5</td>
      <td>POINT Z (-77.13200 3.84917 860.00000)</td>
    </tr>
    <tr>
      <th>11</th>
      <td>12</td>
      <td>-77.14550</td>
      <td>3.84083</td>
      <td>B6A</td>
      <td>Alta</td>
      <td>140</td>
      <td>Buenaventura</td>
      <td>6</td>
      <td>POINT Z (-77.14550 3.84083 140.00000)</td>
    </tr>
    <tr>
      <th>12</th>
      <td>13</td>
      <td>-77.14033</td>
      <td>3.82083</td>
      <td>B7A</td>
      <td>Alta</td>
      <td>20</td>
      <td>Buenaventura</td>
      <td>7</td>
      <td>POINT Z (-77.14033 3.82083 20.00000)</td>
    </tr>
    <tr>
      <th>13</th>
      <td>14</td>
      <td>-77.06194</td>
      <td>3.88972</td>
      <td>B8A</td>
      <td>Alta</td>
      <td>340</td>
      <td>Buenaventura</td>
      <td>8</td>
      <td>POINT Z (-77.06194 3.88972 340.00000)</td>
    </tr>
    <tr>
      <th>6</th>
      <td>7</td>
      <td>-77.06667</td>
      <td>3.86667</td>
      <td>B1B</td>
      <td>Baja</td>
      <td>0</td>
      <td>Buenaventura</td>
      <td>1</td>
      <td>POINT (-77.06667 3.86667)</td>
    </tr>
    <tr>
      <th>7</th>
      <td>8</td>
      <td>-77.06750</td>
      <td>3.89417</td>
      <td>B2B</td>
      <td>Baja</td>
      <td>20</td>
      <td>Buenaventura</td>
      <td>2</td>
      <td>POINT (-77.06750 3.89417)</td>
    </tr>
    <tr>
      <th>8</th>
      <td>9</td>
      <td>-77.08167</td>
      <td>3.89200</td>
      <td>B3B</td>
      <td>Baja</td>
      <td>0</td>
      <td>Buenaventura</td>
      <td>3</td>
      <td>POINT (-77.08167 3.89200)</td>
    </tr>
    <tr>
      <th>9</th>
      <td>10</td>
      <td>-77.11167</td>
      <td>3.86000</td>
      <td>B4B</td>
      <td>Baja</td>
      <td>20</td>
      <td>Buenaventura</td>
      <td>4</td>
      <td>POINT (-77.11167 3.86000)</td>
    </tr>
    <tr>
      <th>10</th>
      <td>11</td>
      <td>-77.13200</td>
      <td>3.84917</td>
      <td>B5B</td>
      <td>Baja</td>
      <td>20</td>
      <td>Buenaventura</td>
      <td>5</td>
      <td>POINT (-77.13200 3.84917)</td>
    </tr>
    <tr>
      <th>11</th>
      <td>12</td>
      <td>-77.14550</td>
      <td>3.84083</td>
      <td>B6B</td>
      <td>Baja</td>
      <td>0</td>
      <td>Buenaventura</td>
      <td>6</td>
      <td>POINT (-77.14550 3.84083)</td>
    </tr>
    <tr>
      <th>12</th>
      <td>13</td>
      <td>-77.14033</td>
      <td>3.82083</td>
      <td>B7B</td>
      <td>Baja</td>
      <td>0</td>
      <td>Buenaventura</td>
      <td>7</td>
      <td>POINT (-77.14033 3.82083)</td>
    </tr>
    <tr>
      <th>13</th>
      <td>14</td>
      <td>-77.06194</td>
      <td>3.88972</td>
      <td>B8B</td>
      <td>Baja</td>
      <td>0</td>
      <td>Buenaventura</td>
      <td>8</td>
      <td>POINT (-77.06194 3.88972)</td>
    </tr>
  </tbody>
</table>
</div>




```python
print("Diferencias entre Bahías")
print(stats.mannwhitneyu(Malaga['ABUN'], Buenaventura['ABUN'], use_continuity=True, alternative='two-sided', axis=0, method='auto'))


```

    Diferencias entre Bahías
    MannwhitneyuResult(statistic=114.0, pvalue=0.41105724999199456)
    
