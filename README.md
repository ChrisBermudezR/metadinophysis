# Material suplementario del artículo: PRIMER REGISTRO DEL GÉNERO DE DINOFLAGELADO TECADO METADINOPHYSIS NIE & WANG (DINOPHYCEAE: DINOPHYSIALES) EN ÁREAS COSTERAS DEL PACIFICO COLOMBIANO Y SU RELACIÓN CON LAS CONDICIONES HIDROGRÁFICAS



## Materiales y Métodos

### Área de estudio

![Figura suplementaria 01](/Imagenes/Fig_Supp_01.png)
**Figura suplementaria 1.** Posición de los mareógrafos de la Red de Parámetros Oceanográficos y de Meteorología Marina (Redpomm). Los datos de mareas analizados fueron tomados del mareógrafo de Isla Palma. 

### Resultados 

#### Análisis de abundancia

Los análisis de abundancia se pueden consultar en el siguiente [Cuaderno de Jupyter.](/Analisis/Analisis_Metadinophysis.ipynb)

Los análisis de cómo se contruyó la gráfica de la onda mareal para el área de estudio se pueden consultar en este [script](/Analisis/analisis_Mareal.R)
